#!/bin/bash

function usage {
	echo "Usage: $0 [--silent] dev|prod"
	exit 1 
}

PAUSE=1
ENV=$1
if [ $# -lt 1 ]
then
	usage
fi

if [ $# -gt 1 ]
then
	case $1 in
		"--silent")
			PAUSE=0
			ENV=$2
			;;
		*)
			usage
			;;
	esac
fi

if [ $PAUSE -eq 1  ]
then
	echo "WARNING!!!!! THIS WILL OVERWRITE YOUR CURRENT Dockerfile!!!!!! CTRL-C now if you don't want this..."
	echo "Currently creating Dockerfile for environment: $1"
	read
else
	echo "Overwriting previous Dockerfile with new $ENV version..."
fi

cat Dockerfile.template Dockerfile.$ENV >devBuild/Dockerfile
exit 0

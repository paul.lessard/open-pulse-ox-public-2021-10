#!/bin/bash

function usage {
	echo "Usage: $0 [container_name|--all] dest_directory"
	echo "If your first arg is --all this script will export the logs from all docker containers matching openpulseox, and put the files under a dir named after the container it came from."
	exit 1

}

function getLogs() {
	timestamp=`date +%-Y%m-%d_%H-%M-%S`
	dir="$2/$timestamp/"
	mkdir -p $dir
	echo "Writing uwsgi from $1 to Dir $dir"
	docker cp  $1:/var/log/uwsgi/uwsgi.log $dir
	echo "Writing nginx from $1 to Dir $dir"
	docker cp  $1:/var/log/nginx/access.log $dir
	docker cp  $1:/var/log/nginx/error.log $dir
	echo "Writing supervisord from $1 to Dir $dir"
	docker cp  $1:/var/log/supervisord/supervisord.log $dir
}

function dumpAll {
	tainers=`docker ps --all --filter ancestor=openpulseox --format "{{.Names}}"`
	for con in $tainers;
	do
		subdir="$1$con"
		getLogs $con $subdir
	done
	exit 0
}

if [ $# -lt 2  ];
then
	usage
fi

case $1 in
	--all)
		dumpAll $2
		;;
	*)
		usage
		;;
esac

getLogs $1 $2 

#!/bin/bash
function usage {
	echo "Attempts to extract a text record and base64 decode it.  ATM, this script only decodes the LAST tEXt chunk in the PNG file."
	echo "$0 image_file output_file"
}

if [ $# -lt 2 ];
then
	usage
	exit 1
fi

if which pngcheck 1>/dev/null 2>&1;
then
	pngcheck -qt $1 | tail -n 1 | xargs | base64 -d >$2
else
	echo "pngcheck does not seem to be installed.  Please install it before using this command."
	echo "on Ubuntu you can get it from apt with: sudo apt-get install pngcheck"
	exit 1
fi

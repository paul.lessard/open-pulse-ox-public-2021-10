#!/bin/bash

#Attempts to find a running openpuseox docker container, then executes the internal update story function.
#Note:  This only works with a DEV docker container that has a valid /mnt *inside* the docker container.
docker exec `docker ps | grep openpulseox | awk '{print $1}'` /devtest.sh dev-update-story

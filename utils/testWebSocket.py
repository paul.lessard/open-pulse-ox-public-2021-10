#!/usr/bin/env python

import sys
import argparse
import asyncio

try:
    from websockets import connect
except Exception as e:
    print(f"You do not appear to have websockets python module currently installed.")
    print(f"Don't know your config, but perhaps try:\n\n\tpip3 install websockets\n")
    sys.exit(1)


parser = argparse.ArgumentParser(description='Connect to an OpenPulseOx webSocket Plugin.')
parser.add_argument('uri', help='The URI of the websocket.  Should start with ws:// or wss://')

args = parser.parse_args()

async def hello(uri):
    async with connect(uri) as websocket:
        await websocket.send("Hello world!")
        msg = await websocket.recv()
        print(f'Message from server: {msg}')

asyncio.run(hello(args.uri))

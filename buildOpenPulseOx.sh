#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

echo "Script directory is: $SCRIPT_DIR"
function usage {
	echo "Usage: $0 [--clean|--install-build-deps|--generate-dev|--generate-prod]"
	exit 1
}

function installDeps {
	source dependencies.txt
	exit 0
}

function clean {
	rm -fR $SCRIPT_DIR/devBuild && mkdir -p $SCRIPT_DIR/devBuild
	#OpenPulseOx API Prework
	python3 -m venv $SCRIPT_DIR/devBuild/api/openpulseox-venv
	exit 0
}


function checkPip3 {
	echo -n "PIP: Testing if $1 is installed: "
	pip3 list|grep "$1" 1>/dev/null 2>&1;
	if [ $? -eq 0 ]; 
	then 
		echo "Installed!";
	else
		echo "CRITICAL DEPENDENCY NOT INSTALLED IN HOST!  :("
		echo
		echo "Please install the required dependency above before proceeding."
		echo
		exit 1
	fi

}

function preFlightCheck {
	echo "Making sure devBuild is created..."
	mkdir -p $SCRIPT_DIR/devBuild 
	
	checkPip3 "Pillow"
	checkPip3 "mysql-connector"
	echo "Environment is now set up."

}

preFlightCheck

if [ $# -gt 0 ];
then
	case $1 in
		"--clean") 
			clean
			;;
		"--generate-dev") ./generateContainerDockerfile.sh --silent dev
			;;
		"--generate-prod") ./generateContainerDockerfile.sh --silent prod
			;;
		"--install-build-deps") 
			installDeps
			;;
		*) usage
			;;
	esac
else
	usage
fi

cd $SCRIPT_DIR

cp -r $SCRIPT_DIR/api $SCRIPT_DIR/devBuild/
cp -r $SCRIPT_DIR/ui $SCRIPT_DIR/devBuild/
cp -r $SCRIPT_DIR/story $SCRIPT_DIR/devBuild/
cp -r $SCRIPT_DIR/story/plugins/api/* $SCRIPT_DIR/devBuild/api/src/openpulseox_api/plugins
if [ -f $SCRIPT_DIR/story/plugins/api/plugin_requirements.txt ];
then
	cat $SCRIPT_DIR/story/plugins/api/plugin_requirements.txt >>$SCRIPT_DIR/devBuild/story/requirements.txt
fi
cp -r $SCRIPT_DIR/story/plugins/ui/* $SCRIPT_DIR/devBuild/ui/src/plugins/
cp -r $SCRIPT_DIR/system $SCRIPT_DIR/devBuild/

python3 $SCRIPT_DIR/story/Storymaker.py --placeholders --flags $SCRIPT_DIR/story/ --base-dir $SCRIPT_DIR/devBuild/

source $SCRIPT_DIR/devBuild/api/openpulseox-venv/bin/activate && pip3 install setuptools wheel && \
	cd $SCRIPT_DIR/devBuild/api && python3 ./setup.py bdist_wheel


#OpenPulseOx UI Prework
cd $SCRIPT_DIR/devBuild/ui/
npm install react-scripts
npm run build
cd build && tar -czf $SCRIPT_DIR/devBuild/ui/openpulseox-ui.tgz *

cd $SCRIPT_DIR
#Build docker container
docker build -t "openpulseox:latest" devBuild/


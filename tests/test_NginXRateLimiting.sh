
if [ $# -lt 2 ]
then
	echo "Usage: $1 uri rate"
	echo "\turi\t-\tThe URI to curl.  It will be curled in background."
	echo "\trate\t-\tNumber of requests per second to test."
	echo "Output will be logged to stdout."
	exit 1
fi

rate=`awk "BEGIN {print (1/$2) }"`

echo "Sending requests to $1 at a rate of $2 per second. ($rate)"
echo "Press CTRL-C to stop."

count=0

function getURI {
	status_code=`curl -v $1 2>&1 | grep "< HTTP" | awk '{ print $3}' `
	echo "Count $count - $status_code"
}

while [ true ]
do 
	getURI $1
	sleep $rate
	count=`awk "BEGIN {print ($count + 1)}"`
done

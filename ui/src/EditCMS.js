import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Icon from '@material-ui/core/Icon';
import Alert from '@material-ui/lab/Alert';
import Switch from '@material-ui/core/Switch';
import { withStyles } from "@material-ui/core/styles";
import MDEditor from '@uiw/react-md-editor';

import styles from './style';

import AccountCircle from '@material-ui/icons/AccountCircle';

class EditCMS extends React.Component {
	constructor(props) {
		super(props);
		this.state = { 
			message: null,
			cms_topic: "-1",
			cms_pages: null,
			classes: this.props.classes,
			cms_markdown: "",
			cms_nav_order: "",
			cms_nav_text: "",
			cms_nav_icon: -1,
			cms_nav_link: "",
			cms_nav_enabled: true,
			new_cms_topic: ""
		};
	}

	componentDidMount() {
		fetch('/api/getCMSPagesList')
		.then(results => results.json())
		.then( (results) => {
			this.setState({ cms_pages: results })
		}
		)
	}

	handleChange = (e) => {
		this.setState({ [e.target.name] : e.target.value })
	}

	handleCMSChange = (e) => {
		this.setState({ [e.target.name] : e.target.value })
		if (e.target.value !== "-1") {
			fetch('/api/getCMSCurrentPage?page_name='+e.target.value)
			.then(results => results.json())
			.then(results => {
				this.setState({
					cms_markdown: results.markdown,
					cms_nav_order: results.nav_order,
					cms_nav_text: results.nav_text,
					cms_nav_icon: results.nav_icon,
					cms_nav_link: results.nav_link,
					cms_nav_enabled: results.enabled === 1 ? true : false 
				})
			})
		}else{
			this.setState({
				cms_markdown: "",
				cms_nav_order: "",
				cms_nav_text: "",
				cms_nav_icon: "-1",
				cms_nav_link: "",
				cms_nav_enabled: true 
			})
		}
	}

	handleCheckedChange = (e) => {
		this.setState({ [e.target.name] : e.target.checked })
	}

	handleSelectChange = (e) => {
		this.setState({ [e.target.name] : e.target.value })
	}

	handleMDChange = (value) => {
		this.setState({ cms_markdown : value  })
	}

	handleSaveCMS = (e) => {
		e.preventDefault();
		fetch('/api/saveCMS', {
			headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
			method: 'POST',
			body: JSON.stringify({ 
				new_cms_topic: this.state.new_cms_topic,
				cms_topic: this.state.cms_topic,
				cms_markdown: this.state.cms_markdown,
				cms_nav_order: this.state.cms_nav_order,
				cms_nav_text: this.state.cms_nav_text,
				cms_nav_link: this.state.cms_nav_link,
				cms_nav_icon: this.state.cms_nav_icon,
				cms_nav_enabled: this.state.cms_nav_enabled
			})
		})
		.then(res => res.json() )
		.then( (res) => {
			if (res.success) {
				this.setState({ "message": { text: "Save successful!", type: "success"}})
			}else{
				this.setState({ "message": { text: "Save FAILED!", type: "warning"}})
			}
		})

	}

	render(){
		if (this.state.cms_pages !== null) {
		return (
			<div className={ this.state.classes.page_margin}>
			<form onSubmit={this.handleSaveCMS}>
			<FormControl className={this.state.classes.formControl} variant="outlined" >
				<InputLabel id="cms_topic_inputlabel">CMS Topic</InputLabel>
				<Select
					id="cms_topic"
					name="cms_topic"
					value={this.state.cms_topic}
					onChange={this.handleCMSChange} 
				>
					<MenuItem value="-1" key="-1">New</MenuItem>
					{ this.state.cms_pages.map((value, index) => {
						return <MenuItem value={value} key={index} >{value}</MenuItem>;
						})
					}
				</Select>
			</FormControl>
			<Divider />
			{ this.state.cms_topic === "-1" && 
			<FormControl className={this.state.classes.formControl} variant="outlined">
				<TextField name="new_cms_topic" id="outlined-basic" label="New CMS Topic" value={this.state.new_cms_topic} onChange={this.handleChange}/>
			</FormControl>
			}
			<FormControl>
				<FormControlLabel
			        	control={
					<Switch
						checked={this.state.cms_nav_enabled}
					        onChange={this.handleCheckedChange}
					        color="primary"
					        name="cms_nav_enabled"
				        	inputProps={{ 'aria-label': 'primary checkbox' }}
					/>
					}
					label="Enabled"
					labelPlacement="top"
				/>
			</FormControl>

			<br/>
			<FormControl className={this.state.classes.formControl} style= {{ width: '100%' }} variant="outlined">
				<MDEditor name="cms_markdown" id="outlined-basic" label="Page Markdown" onChange={this.handleMDChange} value={this.state.cms_markdown}/>
			</FormControl>
			<FormGroup row>
			<FormControl className={this.state.classes.formControl} variant="outlined">
				<TextField name="cms_nav_text" id="outlined-basic" label="Navigation Label" value={this.state.cms_nav_text} onChange={this.handleChange}/>
			</FormControl>
			<FormControl className={this.state.classes.formControl} variant="outlined">
				<TextField name="cms_nav_order" id="outlined-basic" label="Navigation Order" value={this.state.cms_nav_order} onChange={this.handleChange} />
			</FormControl>
			<FormControl className={this.state.classes.formControl} variant="outlined">
				<InputLabel>Navigation Icon</InputLabel>
				<Select name="cms_nav_icon" id="outlined-basic" value={this.state.cms_nav_icon} onChange={this.handleChange}>
					<MenuItem value="-1">No Icon</MenuItem>
					<MenuItem value="0" >Account Circle</MenuItem>
				</Select>
			</FormControl>
			<FormControl className={this.state.classes.formControl} variant="outlined">
				<TextField name="cms_nav_link" id="outlined-basic" label="Navigation Link" value={this.state.cms_nav_link} onChange={this.handleChange}/>
			</FormControl>
			</FormGroup>
			<Button variant="contained" color="primary" type="submit">Save</Button>
			</form>



			{ this.state.message !== null &&
				<Alert severity={ this.state.message.type}>{ this.state.message.text }</Alert>
			}
			</div>
		);}else return null;
	}
}

export default withStyles(styles, { withTheme: true}) (EditCMS);

import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {List} from '@material-ui/core';
import {ListItem} from '@material-ui/core';
import {ListSubheader} from '@material-ui/core';
import Card from '@material-ui/core/Card';
import {CardContent} from '@material-ui/core';
import {Typography} from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import Icon from '@material-ui/core/Icon';
import Alert from '@material-ui/lab/Alert';
import Switch from '@material-ui/core/Switch';
import { withStyles } from "@material-ui/core/styles";
import MDEditor from '@uiw/react-md-editor';

import styles from './style';
import AccountCircle from '@material-ui/icons/AccountCircle';

class Profile extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			current_profile: null
			,classes: this.props.classes
		}
	}

	componentDidMount() {
		fetch('/api/getCurrentProfile')
		.then(results => results.json())
		.then( (results) => {
			if (results.success) {
				this.setState({ current_profile: results.profile })
			}
		}
		)
	}

	handleChange = (e) => {
		this.setState({[e.target.name]:e.target.value})
	}

	render() {
		return (
		<Card>
		<CardContent align="center">
			{ this.state.current_profile !== null &&
			<form>
				<List
				      subheader={
				              <ListSubheader component="div" id="nested-list-subheader">
				                Profile for user {this.state.current_profile.username}
				              </ListSubheader>
				      }
				>
				<ListItem><TextField id="outlined-basic" name="first_name" label="First Name" value={this.state.current_profile.first_name} onChange={this.handleChange} variant="outlined"/>
				<TextField id="outlined-basic" name="last_name" label="Last Name" value={this.state.current_profile.last_name} onChange={this.handleChange} variant="outlined" /></ListItem>
				<ListItem><TextField id="outlined-basic" name="notes" label="Notes" value={this.state.current_profile.notes} onChange={this.handleChange} variant="outlined" width={200} height={300} /></ListItem>
				</List>
			</form>
			}
		</CardContent>
		</Card>
		);
	}
}

export default withStyles(styles, { withTheme: true}) (Profile);

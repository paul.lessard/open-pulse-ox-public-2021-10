import React from 'react';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Redirect } from 'react-router-dom';
import Alert from '@material-ui/lab/Alert';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';

class SignIn extends React.Component {
	constructor(props) {
		super(props);
		this.state = { 
			formAction: "/api/login",
			dialog: true,
			message: null
		};
	}

        onChange(e) {
		this.setState({ [e.target.name]: e.target.value });
	}


	onSubmit(e) {
		e.preventDefault();

		console.log("Username: "+this.state.username)
		console.log("Password: "+this.state.password)
		fetch(this.state.formAction, {
			headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
			method: 'POST',
			body: JSON.stringify({ 
				username: this.state.username,
				password: this.state.password
			})
		})
		.then(res => res.json() )
		.then( (res) => {
			if (res.success) {
				sessionStorage.setItem("authed", true);
				sessionStorage.setItem("hiddenFlag", "FLAG{PLACEHOLDER03}");
				console.log("From is: "+this.state.from);
				this.setState({ dialog: false });
				return(<Redirect to={this.state.from}/>);
			}else{
				this.setState({ message: { text: "Invalid username and/or password.", type: "warning"} })
				sessionStorage.setItem("authed", false);
			}
		})
	}

	render(){
		let authed = sessionStorage.getItem("authed") === "true" || false;
		console.log("Authed: "+authed);

		if (authed === false){
			return (
			<Dialog open={this.state.dialog}>
			<DialogTitle id="simple-dialog-title">Please Sign In</DialogTitle>
			<Card>
			<form onSubmit={this.onSubmit.bind(this)}>
				<TextField name="username" label="Username" onChange={this.onChange.bind(this)} />
				<br/>
				<TextField name="password" label="Password" type="password" onChange={this.onChange.bind(this)} />
				<br/>
				<Button type="submit">Login</Button>
			</form>
				{ this.state.message !== null &&
					<Alert severity={this.state.message.type}>{this.state.message.text}</Alert>
				}
			</Card>
			</Dialog>);
		}else{
			return(<Redirect to={this.state.from}/>);
		}
	}
}

export default SignIn;

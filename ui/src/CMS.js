import React from 'react';
import { Remarkable } from 'remarkable';

class CMS extends React.Component {
	constructor(props) {
		super(props);
		this.state = { 
			page_content: null,
			classes: this.props.classes
		};
	}

	componentDidMount() {
	   	let page_name = this.props.match.params.page_name;

		fetch('/api/getCMSCurrentPage?page_name='+page_name)
		.then(results => results.json())
		.then( (results) => {
			this.setState({ page_content: results })
		})
	}

	render(){
		if (this.state.page_content != null) {
			console.log("CMS Data:");
			console.log(this.state.page_content);
			console.log(this.state.classes);
			let md = new Remarkable();
			var content = md.render(this.state.page_content.markdown);
			return (
				<div dangerouslySetInnerHTML={{__html:content}} />
			);
		} else return null;
	}
}

export default CMS;

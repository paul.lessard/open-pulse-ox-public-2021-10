import React from 'react';
import * as d3 from "d3";

class PulseOxDetails extends React.Component {
	constructor(props){
	   super(props);
	   this.myRef = React.createRef();
	   this.margin = ({top: 30, right: 0, bottom: 30, left: 40})
	}




	componentDidMount(){
	   let ox_id = this.props.match.params.id;

	   fetch(`/api/getPulseOxDetails?ox_id=${encodeURIComponent(ox_id)}`).then(res => res.json())
	   .then( (results) => {
		   if (results.length === 0) return;
	   	   //let dataset = [100, 200, 300, 400, 500];
	   	   let dataset = results.map( (record) => record.oxygen_level * 100);
		   console.log(dataset);

		   // set the dimensions and margins of the graph
		   let margin = {top: 10, right: 30, bottom: 90, left: 40};
		   let size = 500;
		   let width = size - margin.left - margin.right;
		   let height = size - margin.top - margin.bottom;

		   let svg = d3.select(this.myRef.current)
	               .append('svg')
	               .attr('width', width)
	               .attr('height', height);
		   let rect_width = 95;


		  // Add Y axis
		  var y = d3.scaleLinear()
		   .domain([0, 100])
		   .range([ height, 10]);
		  svg.append('svg')
		   .call(d3.axisLeft(y));


		   svg.selectAll('rect')
		      .data(dataset)
		      .enter()
		      .append('rect')
		      .attr('x', (d, i) => 5 + i*(rect_width + 5))
		      .attr('y', d => height - d)
		      .attr('width', rect_width)
		      .attr('height', d => d)
		      .attr('fill', 'teal');
		},

		(error) => {

		}
	   )

	 }


	render(){
		console.log(this.props);
	  return <div>
			<h2>PulseOx Details</h2>
			<h4>Oximeter ID: {this.props.match.params.id}</h4>
			<div ref={this.myRef} />
		</div>
	}
}

export default PulseOxDetails;

import React from 'react';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {
	  Link
} from "react-router-dom";

class PulseOxList extends React.Component {

	constructor(props){
		super(props)
		this.state = { button_comps: <br/> };
	}
	
	mapButtons(button){
	  return (
		<Grid item key={button.id} xs={6}>
  		<Card bgcolor="secondary.main" >
		  <CardContent>
		  	<Typography variant="body1" color="textSecondary">SN: {button.serialnumber}</Typography>
		  	<Typography variant="body2">Name: {button.name}</Typography>
		  </CardContent>
		  <CardActions>
		    <Link to={`/pulseoxdetails/${button.id}`}>
	  		<Button variant="contained" color="primary" >Details</Button>
		    </Link>
		  </CardActions>
		</Card>
		</Grid>
	  )
	}	

	componentDidMount() {
	  fetch("/api/getPulseOxList")
	  .then(buttons => buttons.json())
	  .then( (buttons) => {	  
		  console.log(buttons)
		  let comps = buttons.map(this.mapButtons)
		  this.setState({ button_comps: comps })
	  	},
		(error) => {
		}
	  );
	}

	render(){
	console.log("Button COMPS: "+this.state.button_comps)
	  return <div>
			<h2>PulseOxList</h2>
			<Container maxWidth="md">
			  <Grid container spacing={2}>
			  	{this.state.button_comps}
			  </Grid>
			</Container>
		 </div>
	}
}

export default PulseOxList;

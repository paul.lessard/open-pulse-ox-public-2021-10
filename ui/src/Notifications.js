import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import {CardContent} from '@material-ui/core';
import {Typography} from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import Icon from '@material-ui/core/Icon';
import Alert from '@material-ui/lab/Alert';
import Switch from '@material-ui/core/Switch';
import { withStyles } from "@material-ui/core/styles";

import styles from './style';
import AccountCircle from '@material-ui/icons/AccountCircle';

class Notifications extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			current_notifications: null
			,classes: this.props.classes
		}
	}

	componentDidMount() {
	}

	handleChange = (e) => {
		this.setState({[e.target.name]:e.target.value})
	}

	render() {
		return (
		<Card>
			<CardContent align="center">
			        <Typography className={this.state.classes.title} color="textSecondary" gutterBottom>
			          You have no notifications!
			        </Typography>
			</CardContent>
		</Card>
		);
	}
}

export default withStyles(styles, { withTheme: true}) (Notifications);

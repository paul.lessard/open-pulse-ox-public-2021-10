import React from 'react';
import Badge from '@material-ui/core/Badge';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Drawer from '@material-ui/core/Drawer';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import { withStyles } from "@material-ui/core/styles";
import {TextField} from '@material-ui/core';
import clsx from 'clsx';
import styles from './style';
import Profile from './Profile';
import Notifications from './Notifications';

//ICONS
import IconButton from '@material-ui/core/IconButton';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import TrendingDownIcon from '@material-ui/icons/TrendingDown';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import DeviceUnknownIcon from '@material-ui/icons/DeviceUnknown';

class NavBar extends React.Component{
	constructor(props) {
		super(props);
		this.state = { 
			anchorEl: null
			,search_results_anchor: null
			,menuId: 'primary-search-account-menu'
			,classes: this.props.classes
			,drawerOpen: false
			,nav_list: null
			,notifications: 1
			,notifications_dialog: false
			,profile_dialog: false
			,authed: false
			,current_search: ""
			,search_results: null
		};
		setTimeout(this.checkNotifications, 10);
		setTimeout(this.checkAuthed, 100);
	}

	checkAuthed = () => {
		let authed = sessionStorage.getItem("authed") === "true" || false;
		this.setState({ authed: authed })
		setTimeout(this.checkAuthed, 500)
	}

	handleDrawerOpen = () => {
		this.setState({drawerOpen: true});
	};

	handleDrawerClose = () => {
		this.setState({drawerOpen: false});
	};

	isMenuOpen(){
		return Boolean(this.state.anchorEl);
	}

	handleMenuOpen(event){
		this.setState({ anchorEl: event.currentTarget});
	}

	handleMenuClose(event) {
		this.setState({ anchorEl: null });
	}

	handleSearchResultsClose(event){
		this.setState({ search_results_anchor: null });
	}

	openNotifications = (e) => {
		this.setState({ notifications_dialog: true });
	}

	closeNotifications = (e) => {
		this.setState({ notifications_dialog: false });
	}

	checkNotifications = () => {
		let notifications = (Math.random() * 10) % 3;

		this.setState({ notifications: (this.state.notifications + Math.trunc(notifications)) })

		let delay = Math.random() * 15000;
		setTimeout(this.checkNotifications, delay)
	}

	openProfile = (e) => {
		this.setState({ profile_dialog: true });
	}

	closeProfile = (e) => {
		this.setState({ profile_dialog: false });
	}

	search = (e) => {
		this.setState({ current_search: e.target.value });
		if (e.target.value.length > 2) {
			fetch('/api/vuln/searchCMS?name='+e.target.value)
			.then(results => results.json())
			.then( (results) => {
				console.log(results);
				this.setState({ 
					search_results: results.result
					,search_results_anchor: e.target
				})
			})
		}
	}

	refreshLinks = () => {
		fetch('/api/getNavLinks')
		.then(results => results.json())
		.then( (results) => {
			console.log(results);
			if (results !== this.state.nav_list)
				this.setState({ nav_list: results})
		})
		let nots = this.state.notifications + 1;
		this.setState({notifications:nots});
	}

	componentDidMount(){
		this.refreshLinks();
		this.timer = setInterval(this.refreshLinks, 5000);
	}

	getNavIcon(index) {
		let icon = this.state.nav_list[index].nav_icon
		switch (icon) {
			case 0: return <AccountCircle/>
			case 1: return <MailIcon/>
			case 2: return <MoreIcon/>
			case 3: return <NotificationsIcon/>
			case 4: return <TrendingDownIcon/>
			case 5: return <TrendingUpIcon/>
			case 6: return <SearchIcon/>
			default: return <DeviceUnknownIcon />
		}
	}

	render(){
		let searchResults = (
			<Menu
				anchorEl={this.state.search_results_anchor}
				anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
				id="search_results"
				transformOrigin={{ vertical: 'top', horizontal: 'left' }}
				open={this.state.search_results !== null}
				onClose={this.handleSearchResultsClose}
			>
			{ this.state.search_results !== null &&  this.state.search_results.map((record, index) => (
				<MenuItem button key={record[0]} component="a" href={record[8]}>
					{record[1]}
				</MenuItem>
			))}
			</Menu>

		);
		let renderMenu = (

		      <Menu
		        anchorEl={this.state.anchorEl}
	        	anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
		        id={this.state.menuId}
		        keepMounted
	        	transformOrigin={{ vertical: 'top', horizontal: 'right' }}
		        open={this.isMenuOpen()}
		        onClose={this.handleMenuClose.bind(this)}
		      >
		        <MenuItem onClick={this.openProfile}>Profile</MenuItem>
	        	<MenuItem component={Link} onClick={this.handleMenuClose.bind(this)}  to="/logout" >
				Logout
			</MenuItem>
		      </Menu>
			);
		
		const appBar =  (
		      <div className={this.state.classes.grow}>
			{//<CssBaseline /> 
			}
		        <AppBar 
			        position="fixed"
			        className={clsx(this.state.classes.appBar, {
					          [this.state.classes.appBarShift]: this.state.drawerOpen,
					        })}
			>
		        <Toolbar>
		            <IconButton
		              edge="start"
			      className={clsx(this.state.classes.menuButton, this.state.drawerOpen && this.state.classes.hide)}
		              color="inherit"
		              aria-label="open drawer"
			      onClick={this.handleDrawerOpen}
		            >
		              <MenuIcon flag="FLAG{PLACEHOLDER06}" />
		            </IconButton>
		            <Typography className={this.state.classes.title} variant="h6" noWrap>
		              Open PulseOx
		            </Typography>
		            <div className={this.state.classes.grow} />
			    <div>
				<TextField placeholder="Search..." variant="filled" onChange={this.search} />
			    </div>
		            <div className={this.state.classes.grow} />
		            <div className={this.state.classes.sectionDesktop}>
		              <IconButton aria-label="show new notifications" color="inherit" onClick={this.openNotifications}>
		                <Badge badgeContent={this.state.notifications} color="secondary">
		                  <NotificationsIcon />
		                </Badge>
		              </IconButton>
		              <IconButton
		                edge="end"
		                aria-label="account of current user"
		                aria-controls={this.state.menuId}
		                aria-haspopup="true"
		                onClick={this.handleMenuOpen.bind(this)}
		                color="inherit"
		              >
		                <AccountCircle />
		              </IconButton>


		            </div>
		        </Toolbar>
		        </AppBar>
			{ this.state.nav_list !== null && 
			<Drawer
			        className={this.state.classes.drawer}
			        variant="persistent"
			        anchor="left"
			        open={this.state.drawerOpen}
			        classes={{ paper: this.state.classes.drawerPaper, }} >
				<div className={this.state.classes.drawerHeader}>
			        	<IconButton onClick={this.handleDrawerClose}>
						{this.props.theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
					</IconButton>
			        </div>
			        <Divider />
				<List>
					{this.state.nav_list.length > 0 &&  this.state.nav_list.map((record, index) => (
						<ListItem button key={record.nav_text} component="a" href={record.nav_link}>
							<ListItemIcon>{this.getNavIcon(index)}</ListItemIcon>
							<ListItemText primary={record.nav_text} />
						</ListItem>
					))}
			        	<Divider />
					{/*
					<ListItem button key="EditCMS" component="a" href="/EditCMS">
						<ListItemText>EditCMS</ListItemText>
					</ListItem>*/
					}
					<ListItem button key="PulseOxList" component="a" href="/pulseoxlist">
						<ListItemText>Pulse Oximeter List</ListItemText>
					</ListItem>
			        </List>
			</Drawer>
			}
		        { searchResults }
		        { this.state.authed && renderMenu }
			<Dialog open={this.state.notifications_dialog} fullWidth={true} maxWidth='md'>
				<Notifications />
				<Button onClick={this.closeNotifications}>Close</Button>
			</Dialog>

			<Dialog open={this.state.profile_dialog} fullWidth={true} maxWidth='md'>
				<Profile />
				<Button onClick={this.closeProfile}>Close</Button>
			</Dialog>
		      </div>
		);
		return appBar;
	}
}

export default withStyles(styles, { withTheme: true}) (NavBar);

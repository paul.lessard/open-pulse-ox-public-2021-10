import logo from './logo.svg';
import './App.css';
import React from 'react';
import NavBar from './AppNavBar';
import {
	  BrowserRouter,
	  Switch,
	  Route,
	  Link,
	  Redirect
} from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import styles from './style';
import EditCMS from './EditCMS';
import Home from './Home';
import CMS from './CMS';
import SignIn from './SignIn';
import Logout from './Logout';
import PulseOxList from './PulseOxList';
import PulseOxDetails from './PulseOxDetails';
import Paper from '@material-ui/core/Paper';

function PrivateRoute ({component: Component, ...rest}) { 
	let authed = sessionStorage.getItem("authed") || false;
	console.log("Authed: "+authed);
	return ( 
		<Route {...rest} render={(props) => {
			authed = sessionStorage.getItem("authed") === "true" || false;
			if (authed) return (<Component {...props} /> );
			else return(<Redirect to='/SignIn' />);
			//else return(<Redirect to={{pathname: '/SignIn', state: {from: props.path}}} />);
		}}
		/>
	) 
}


class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			classes: this.props.classes
		}
	}
	render(){
		console.log(this.props);
		console.log(this.state.classes.toolbar);
		  return (
			      <BrowserRouter>
			  	<NavBar/>
			  	<Paper>
			  	<div className={this.state.classes.toolbar} />
			          <Switch>
			            <Route path="/editCMS" component={EditCMS} />
			            <Route path="/CMS/:page_name" component={CMS} />
			            <PrivateRoute path="/pulseoxlist" component={PulseOxList} />
			  	    <PrivateRoute path="/pulseoxdetails/:id" component={PulseOxDetails}/>
			  	    <Route path="/SignIn" component={SignIn} />
			  	    <Route path="/Logout" component={Logout} />
			            <Route path="/" component={Home} />
			          </Switch>
			  	</Paper>
			      </BrowserRouter>
		  )
	}
}

export default withStyles(styles, { withTheme: true}) (App);

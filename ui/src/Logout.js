import React from 'react';
import { Redirect } from 'react-router-dom';

class Logout extends React.Component {
	componentDidMount() {
		fetch('/api/logout')
		.then(props => sessionStorage.setItem("authed", false));
	}

	render(){
		return(<Redirect to="/"/>);
	}
}

export default Logout;

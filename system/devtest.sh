#!/bin/sh

function usage {
	echo "Usage: $0 makestory|start-services|stop-services|go|-h"
	echo "\n"
	echo "\tIf you are in a DEV docker container, you can use these options:"
	echo "\tdev-update-story\t\tUpdates the Storyfile from /mnt"
}

function dumpConfig {
	echo "Dumping Storyfile config.json..."
	cd /opt/openpulseox/story
	python3 Storymaker.py --api-config /opt/openpulseox/api/config.json ./
}

function makestory {
	echo "Populating content from Storyfile..."
	cd /opt/openpulseox/story
	python3 Storymaker.py --db --oximeters --users --pack_images --unix-auth ./

	echo "Copying PNG images into web directory."
	mkdir -p /var/www/html/ui/images
	for filename in `ls -1 /opt/openpulseox/story/images/* 2>/dev/null`
	do
		echo "Copying $filename..."
		cp $filename /var/www/html/ui/images/
		chown nginx:nginx -R /var/www/html/ui/images
	done
}

function stopServices {
	killall supervisord
	echo "Waiting for supervisord to die..."
	sleep 3
}

function restartServices {
	echo "Stopping services..."
	stopServices
	sleep 3
	echo "Starting services..."
	startSupervisord
	sleep 3
}

function updateAPIFromDev {
	rm -fR /opt/openpulseox/api/dev
	mkdir -p /opt/openpulseox/api/dev
	cp -R /mnt/api/* /opt/openpulseox/api/dev/
	rm -fR /opt/openpulseox/api/dev/build
	rm -fR /opt/openpulseox/api/dev/dist
	cd /opt/openpulseox/api/dev
	python3 setup.py bdist_wheel
	pip install --upgrade --force-reinstall dist/openpulseox_api-*.whl
	restartServices
}

function updateUIFromDev {
	rm -fR /opt/openpulseox/ui/dev
	mkdir -p /opt/openpulseox/ui/dev
	cp -R /mnt/ui/* /opt/openpulseox/ui/dev/
	cd /opt/openpulseox/ui/dev
	npm run build
	cd build
	tar -czf /opt/openpulseox/ui/openpulseox-ui.tgz *
	cd /var/www/html/ui/ && tar -xzf /opt/openpulseox/ui/openpulseox-ui.tgz
}

function updateStoryFromDev {
	cd /opt/openpulseox/api/
	unzip openpulseox_api.zip

	mkdir -p /opt/openpulseox/story/
	cp /mnt/story/Storyfile.json /opt/openpulseox/story/Storyfile.json
	dumpConfig

	mariadb openpulseox -b </opt/openpulseox/api/openpulseox_api/sql/setupdb.sql
	makestory
	restartServices
}

function updatePluginsFromDev {
	stopServices
	
	pip install --force-reinstall /mnt/story/plugins/*.whl
	pip install --force-reinstall /mnt/story/plugins/api/*.whl
	pip install --force-reinstall /mnt/story/plugins/ui/*.whl
	cp /mnt/story/plugins/api/*.py /usr/lib/python3/site-packages/openpulseox_api/plugins/

	startSupervisord
}

function startSupervisord {
	supervisord -c /etc/supervisord.conf -l /var/log/supervisord/supervisord.log
}


case $1 in
	"makestory") 
		dumpConfig
		makestory
		;;
	"start-services") 
		startSupervisord
		;;
	"-h" | "--help" | "-?" | "/?") 
		usage
		;;
	"stop-services")
		stopServices
		;;
	"restart-services")
		restartServices
		;;
	"go")
		dumpConfig
		startSupervisord
		echo "Waiting for services to start..."
		sleep 5
		makestory
		;;
	"dev-update-story")
		updateStoryFromDev
		;;
	"dev-update-api")
		updateAPIFromDev
		;;
	"dev-update-ui")
		updateUIFromDev
		;;
	"dev-update-plugins")
		updatePluginsFromDev
		;;
	*) 
		usage
		;;
esac

#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

function usage {
	echo "Usage: $0 [-d|--daemon]"
	echo "
	"
	echo "	-d --daemon		Start with docker container detached.  Good for centralized dev servers.  Operates exactly the same as running this script with no arguments, but the container runs in the background instead of creating an interactive session in the container."
	echo "	-p --private		Start similar to no arguments, but doesn't expose port 80 through your host.  That is, other computers will not be able to directly view the web page unless you you manually make that happen (through ssh tunnel, route forwarding, etc.)."
	echo "	-ls --local-server	Like -p and -d together.  Starts a container in the background but does not expose the container outside of your system (e.g. to the other systems on your network)."
	echo "
	
	
	"
	echo "If run with no arguments the openpulseox container is run interactively, with port 80 exposed, and maps the openpulseox repository to /mnt/ for development updates.  Note:  This will expose the app to anything that can connect to your docker HOST."
}

function background {
	docker run -dit -v "$SCRIPT_DIR:/mnt:ro" -p "80:80" openpulseox /bin/sh
	
	
	con=`docker ps | grep openpulseox | awk '{print $1}'`
	docker exec $con /devtest.sh go
}

function simple {
	docker run -it -v "$SCRIPT_DIR:/mnt:ro" -p "80:80" openpulseox /bin/sh -l
}

function simple_private {
	docker run -it -v "$SCRIPT_DIR:/mnt:ro" openpulseox /bin/sh
}

function background_private {
	docker run -dit -v "$SCRIPT_DIR:/mnt:ro" openpulseox /bin/sh

	con=`docker ps | grep openpulseox | awk '{print $1}'`
	docker exec $con /devtest.sh go
}

function background_nomount {
	docker run -dit -p "80:80" openpulseox /bin/sh

	con=`docker ps | grep openpulseox | awk '{print $1}'`
	docker exec $con /devtest.sh go
}

if [ $# -gt 0  ];then

	case $1 in
		"--daemon" | "-d")
			background
			;;

		"--private" | "-p")
			simple_private
			;;
		"--nomount-server" | "-nms")
			background_nomount
			;;
		"--local-server" | "-ls")
			background_private
			;;
		"-h" | "--help" | "-?" | "/?")
			usage
			;;
		*)
			usage
			;;
	esac
else
	simple
fi


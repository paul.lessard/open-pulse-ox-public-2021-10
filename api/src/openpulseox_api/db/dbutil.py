#Copyright © 2021 Paul Lessard (a.k.a. Battlechef #1666)
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
#associated documentation files (the “Software”), to deal in the Software without restriction, 
#including without limitation the rights to use, copy, modify, merge, publish, distribute, 
#sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or 
#substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
#NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES 
#OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import mysql.connector
from mysql.connector import errorcode
from openpulseox_api.config.config import Config
from openpulseox_api.utils.baseutils import StrEnum
import logging

FORMAT = '%(asctime)-15s %(levelname)s: %(message)s'
logging.basicConfig(format=FORMAT)
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

class DB(StrEnum):
    READ = "database_read"
    WRITE = "database_write"
#    VULN = "vuln_endpoints"
#    VULN_SQLi_PREFIX = "SQLi_"

class DBUtil:
    def __init__(self, config = Config(), database_property = None):
        self.RETRY_MAX = 3
        if isinstance(config, Config):
            #DB Connection Pool Mode...
            self._config = config
            self._db_configs = self._config.get("databases", [])
            self._conns = {}
            self.connectDBs()
        else:
            self._username = self.getDBConfig("username", config = config, database_property = database_property)
            self._password = self.getDBConfig("password", config = config, database_property = database_property)
            self._host = self.getDBConfig("host", config = config, database_property = database_property)
            self._db = self.getDBConfig("db", config = config, database_property = database_property)
            self._conn = self.connect()

    def getDBConfig(self, param, config = None, database_property = None):
        if config is None:
            return self._config.get(param)
        elif database_property is None:
            return config.get(param)
        else:
            return config.get(f"{database_property}.{param}")

    def getDBConn(self, db):
        return self._conns[db]

    def getDBConns(self):
        return self._conns

    def getDBs(self):
        return DB;

    def getDBConfigs(self):
        return self._db_configs

    def connectDB(self, db):
        try:
            un = self.getDBConfigs()[db]["username"]
            pwd = self.getDBConfigs()[db]["password"]
            db_db = self.getDBConfigs()[db]["db"]
            host = self.getDBConfigs()[db]["host"]
            self._conns[db] = mysql.connector.connect(user=un, password=pwd, unix_socket=host, database=db_db)
            log.info(f"Successfully connected db: {db}, using Username: {un} and Database: {db_db}")
        except Exception as e:
            log.warning(f'Failed to connect to database.  DB:{db}.  Exception was: {e}')

    def connectDBs(self):
        try:
            for db in DB:
                self.connectDB(db)
        except Exception as e:
            log.warning(F'Failed to connect to database.  Exception was: {e}')

    def connect(self, params = None):
        un = None
        pwd = None
        sock = None
        db = None
        conn = None
        try:
            if params is not None:
                un = params["username"]
                pwd = params["password"]
                host = params["host"]
                db = params["db"]
            else:
                un = self._username
                pwd = self._password
                host = self._host
                db = self._db

            if "/" in host:
                conn = mysql.connector.connect(user=un, password=pwd, unix_socket=host, database=db)
            else:
                conn = mysql.connector.connect(user=un, password=pwd, host=host, database=db)

        except Exception as e:
                conn = mysql.connector.connect(user=self._username, password=self._password, unix_socket=self._host, database=self._db)
                log.warning(F'Failed to connect to database.  DB:{params["db"]}')
        return conn

    def commit(self):
        self.conn.commit()

    def execDBPoolQuery(self, db, query, params = None, retry_num = 0):
        cursor = None
        try:
            cursor = self.getDBConns()[db].cursor()
            cursor.execute(query, params)
        except mysql.connector.Error as err:
            log.warning(f"MYSQL Error: {err}, attempting to reconnect then retry.")
            self.connectDB(db)
            retry_num += 1
            cursor = self.execDBPoolQuery(db, query, params, retry_num)
        except Exception as e:
            log.warning(f"Failed to execDBPoolQuery.  Exception was: {e}")
        return cursor

    def executeVulnQuery(self, query, params=None, retry_num = 0):
        if retry_num < self.RETRY_MAX:
            try:
                if not hasattr(self, "_conn"):
                    self.connect(params = params)

                cursor = self._conn.cursor()
                cursor.execute(query, params)
            except mysql.connector.InterfaceError as ie:
                log.info(f"User Caused an Interface Error:\n{ie}")
                log.info(f"Explanation: This often happens when the user has attempted a multiline query when only a single line query is permitted, and the like.")
                log.info(f"You will now see a WARNING with the exception they caused.")
                raise ie

            except mysql.connector.ProgrammingError as pe:
                log.info(f"User Caused a Programming Error. Expect this a LOT while CTF participants are probing the database. Here is the error:\n{pe}")
                log.info(f"Explanation: This often happens when the user has caused the SQL query to be invalid (e.g. invalid SQL syntax, and so on).")
                log.info(f"You will now see a WARNING with the exception they caused.")
                raise pe

            except mysql.connector.Error as err:
                log.warning(f"MYSQL Error: {err}, attempting to reconnect then retry.")
                self.connect()
                retry_num += 1
                cursor = self.executeVulnQuery(query,params, retry_num)
        else:
            raise Exception(F"Failed to perform query after {retry_num} retry attempts.")
        return cursor

#Copyright © 2021 Paul Lessard (a.k.a. Battlechef #1666)
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
#associated documentation files (the “Software”), to deal in the Software without restriction, 
#including without limitation the rights to use, copy, modify, merge, publish, distribute, 
#sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or 
#substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
#NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES 
#OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import json, os, sys, logging
import site
from typing import *

FORMAT = '%(asctime)-15s %(levelname)s: %(message)s'
logging.basicConfig(format=FORMAT)
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

class Config:
    def __init__(self, app = None, config_dir = "/opt/openpulseox/api/", filename = None):
        try:
            self._config = self.loadConfig(app = app, config_dir = config_dir, config_file = filename)
        except Exception as e:
            raise Exception(F"Could not load the specified config.json.  Exception was: {e}")

    def getPropertyOf(self, obj: Dict[str,Any], prop_str, default = None):
        config = None
        try:
            tmp_obj = obj
            for prop in prop_str.split("."):
                tmp_obj = tmp_obj.get(prop, default)
                if tmp_obj is None:
                    log.warning(F"Failed to find token {prop} in object tree.")

            config = tmp_obj
        except Exception as e:
            log.warning(F"Unexpected exception occurred while attempting to get property of obj.  Exception was: {e}")
        return config

    def getProperty(self, prop_str, default = None):
        config = self._config
        return self.getPropertyOf(config, prop_str, default=default)

    def get(self, prop_str, default = None):
        return self.getProperty(prop_str, default=default)


    def loadConfig(self, app = None, config_dir = None, config_file = None):
        _config = None
        
        filenames = []
        if app is not None:
            site_dir = str(os.path.join(site.getsitepackages()[0], "openpulseox_api", "config", "config.json"))
            app_dir =  str(os.path.join(app.instance_path, 'config', 'config.json'))
            filenames.append(site_dir)
            filenames.append(app_dir)

        if config_dir is not None:
            api_dir =  str(os.path.join(config_dir, 'config.json'))
            filenames.append(api_dir)
        
        if config_file is not None:
            filenames.append(config_file)

        for filename in filenames:
            try:
                with open(filename) as configFile:
                    _config = json.load(configFile)
                log.info(f'{__name__} successfully loaded config from: {filename}')
                break
            except Exception as e:
                log.warning(F"Failed to load config from {filename}: {e}")

        if _config is None:
            try:
                log.warning("Failed to load config.  Where is it (can't start without it)?")
                _config = Config(input())
            except Exception as e:
                log.critical("Sorry, couldn't load the config.json file anywhere. I have to bail out w/o a config. :(  Trying to exit!  <===== This is really bad, you should pay attention to this!")
                sys.exit(1)
        return _config    


DROP TABLE IF EXISTS openpulseox.cms;
CREATE TABLE openpulseox.cms (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
	,name VARCHAR(255)
	,version INT
	,nav_text VARCHAR(255)
	,nav_icon INT
	,enabled TINYINT
	,markdown TEXT
	,nav_order TINYINT
	,nav_link VARCHAR(255)
);

DROP TABLE IF EXISTS openpulseox.oximeters;
CREATE TABLE openpulseox.oximeters(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
	,serialnumber VARCHAR(255)
	,name VARCHAR(255)
);

DROP TABLE IF EXISTS openpulseox.oximeter_details;
CREATE TABLE openpulseox.oximeter_details(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
	,oximeter_id VARCHAR(255)
	,timestamp TIMESTAMP
	,oxygen_level INT
	,pulse_rate INT
);

DROP TABLE IF EXISTS openpulseox.roles;
CREATE TABLE openpulseox.roles(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
	,rolename VARCHAR(255)
	,oximeter_list TEXT
);

DROP TABLE IF EXISTS openpulseox.users;
CREATE TABLE openpulseox.users(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
	,username VARCHAR(255)
	,role TINYINT
	,password VARCHAR(255)
);

DROP TABLE IF EXISTS openpulseox.profiles;
CREATE TABLE openpulseox.profiles(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
	,userid INT
	,first_name VARCHAR(255)
	,last_name VARCHAR(255)
	,title VARCHAR(255)
	,notes TEXT
);

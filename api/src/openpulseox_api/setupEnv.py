#Copyright © 2021 Paul Lessard (a.k.a. Battlechef #1666)
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
#associated documentation files (the “Software”), to deal in the Software without restriction, 
#including without limitation the rights to use, copy, modify, merge, publish, distribute, 
#sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or 
#substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
#NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES 
#OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import mysql.connector

read_account = os.environ.get("OPENPULSEOX_READ_PW")
write_account = os.environ.get("OPENPULSEOX_WRITE_PW")
conn = mysql.connector.connect(user="root", unix_socket="/run/mysqld/mysqld.sock", database="openpulseox")
conn.autocommit =True 

def exec(query, params=None):
    cursor = conn.cursor()
    cursor.execute(query, params)
    return cursor

exec(F"CREATE USER openpulseox_read@localhost identified by '{read_account}'")
exec(F"CREATE USER openpulseox_write@localhost identified by '{write_account}'")
exec(F"GRANT SELECT on openpulseox.* to openpulseox_read@localhost")
exec(F"GRANT INSERT,UPDATE,SELECT on openpulseox.cms to openpulseox_write@localhost")
exec(F"GRANT INSERT on openpulseox.oximeter_details to openpulseox_write@localhost")

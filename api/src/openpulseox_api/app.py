#Copyright © 2021 Paul Lessard (a.k.a. Battlechef #1666)
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
#associated documentation files (the “Software”), to deal in the Software without restriction, 
#including without limitation the rights to use, copy, modify, merge, publish, distribute, 
#sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or 
#substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
#NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES 
#OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import sys
from flask import Flask, session, redirect, url_for, request, g, abort
from markupsafe import escape
import json
from openpulseox_api.config.config import Config
from openpulseox_api.db.dbutil import DBUtil, DB
from functools import wraps
import hashlib
from art import *
import logging
import traceback
import time, datetime
import base64
import importlib

FORMAT = '%(asctime)-15s %(levelname)s: %(message)s'
logging.basicConfig(format=FORMAT)
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


def create_app(env=None, start_response=None):

    #Utility Functions.

    def configSanityCheck(config):
        success = False
        try:
            dbEdits = False
            if SQLi_INSERT or SQLi_ROOT:
                dbEdits = True


            if dbEdits and not SQUELCH_MULTIPLAYER_DB_EDITS:
                warn = test2art("WARNING")
                log.critical(warn)
                log.critical("You have chosen to enable database edits without using the super memorable config.json parameter:\n\nSQUELCH_MULTIPLAYER_WARNING__I_UNDERSTAND_DB_EDITS_PLUS_PLAYERS_EQUALS_CHAOS\n\nThat means I should tell you that allowing users to compromise a database userid that has edit access (of any kind, such as: UPDATE, DROP, GRANT, INSERT, etc.) will create a potentially unstable game environment for multiple competing players using the same docker container.  If you are using one docker container per player or group you will still likely end up with unstable containers, but at least players will not directly be impacting each other.\n\nAlso worth noting, depending on how skilled the exploiting party is with SQL, they may very well be able to impact the performance of the container if they are inclined to do so.")
            success = True
        except Exception as e:
            log.warning(F"Configuration failed sanity check.  It's broke real bad too.  We threw this exception while testing: {e}")
            success = False
            exit(1)
        return success

    # Begin App Creation
    app = Flask(__name__)
    _config = Config(app = app)
    _dbu = DBUtil(config = _config)
    _dbs = _dbu.getDBs()
    _db_conns = _dbu.getDBConns()
    _vulns = _config.get("vuln_endpoints")

    #Load variables
    DEBUG=_config.get("DEBUG", False)
    LABYRINTH=_config.get("LABYRINTH", False)
    SQLi=_config.get("SQLi", False)
    SQLi_SELECT=_config.get("SQLi_SELECT", False)
    SQLi_UPDATE=_config.get("SQLi_UPDATE", False)
    SQLi_INSERT=_config.get("SQLi_INSERT", False)
    SQLi_ROOT=_config.get("SQLi_ROOT", False)
    SQUELCH_MULTIPLAYER_DB_EDITS=_config.get("SQUELCH_MULTIPLAYER_WARNING__I_UNDERSTAND_DB_EDITS_PLUS_PLAYERS_EQUALS_CHAOS", False)
    ROUTE_PREFIX = _config.get("ROUTE_PREFIX")
    VULN_ROUTE_PREFIX = _config.get("VULN_ROUTE_PREFIX")
    PLUGINS = _config.get("api_plugins", [])
    

    LOADED_PLUGINS = []
    #Load Plugins
    for plugin in PLUGINS:
        try:
            (mod, cls) = plugin.split(":")
            plugin_module = F"openpulseox_api.plugins.{mod}"

            module = __import__(plugin_module, globals(), locals(), [cls], 0)
            plugin_module = getattr(module, cls)
            assert plugin_module is not None

            plugin_loaded = plugin_module(app, _config, plugin, ROUTE_PREFIX)

            LOADED_PLUGINS.append(plugin_loaded)
            log.info(F"Successfully loaded plugin: {plugin}")
        except Exception as e:
            log.warning(F"Failed to load plugin {plugin}.  Exception was: {e}.  Skipping this plugin....")



    # Set the secret key to some random bytes. Keep this really secret!
    app.secret_key = os.urandom(16)


################## Decorators and Before Request #####################

    @app.before_request
    def load_user():
        g.user = session.get("user_id", None)
        if DEBUG:
            log.debug(F"User is: {g.user}")


    def login_required(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if g.user is None:
                return "This endpoint requires an authenticated session"
            return f(*args, **kwargs)
        return decorated_function


    def vulnerable(func):
        @wraps(func)
        def vuln_func(*args, **kwargs):
            try:
                vuln_endpoint = kwargs['vuln_endpoint']
                method = request.method.upper()
                log.debug(F"Vuln Endpoint: {vuln_endpoint}, Method: {method}")

                vuln_endpoints = _config.get(F"vuln_endpoints")
                current_vuln_config = None
                for vuln,vuln_config in vuln_endpoints.items():
                    if vuln_endpoint == vuln_config["endpoint"]:
                        log.debug(F"User is accessing vuln_config {vuln}")
                        current_vuln_config = vuln_config
                
                if current_vuln_config is None: 
                    abort(404)
                
                vuln_args = None
                if method in current_vuln_config["vuln_method"]:
                    if method == "GET":
                        vuln_args = request.args
                        log.debug(F"User supplied GET args: {json.dumps(vuln_args)}")
                    elif method == "POST":
                        vuln_args = request.data
                        log.debug(F"User supplied POST args: {json.dumps(vuln_args)}")

                vuln_query = current_vuln_config["vuln_query"]
                
                #You have to have a z right there or the SQLi doesn't work lol! --|
                #        V---------------------------------------------------------
                vuln_credz = current_vuln_config["vuln_credentials"]
                exploit_fields = current_vuln_config["exploit_fields"]
                vuln_multi_line_sql = current_vuln_config.get("vuln_multi_line_sql", False)

                vuln_dbconn = DBUtil(config = vuln_credz, database_property = None)

                result = func(vuln_dbconn, vuln_endpoint, exploit_fields, vuln_query, vuln_args)
                if result is None:
                    abort(500)
                else:
                    return result
            except Exception as e:
                log.warning(F"Vulnerable wrapper for function '{func.__name__}' caused an exception.  Exception was: {e}")
                trace = traceback.format_exc()
                log.warning(F"Here is the exception the user caused:\n{trace}")
            return lambda x,y: (x,y)
        return vuln_func



    ############### Flask App Routes ################


    @app.route('/', defaults={'u_path': ''})
    @app.route('/<path:u_path>')
    def catch_all(u_path):
        if LABYRINTH:
            labyrinth = text2art("Labyrinth", font="random")
            welcome = F"Welcome to the\n\n{labyrinth}\n\nEnjoy your stay...\n{repr(u_path)}\n"
            motd = F"""
                Join the BioHacking Village at the next conference to navigate the Labyrinth!
                \n\n
                \tWhat is the Labyrinth?
                \tThe Script Labyrinth is a maze you must navigate by script.  Once you start running the Labyrinth, you must answer each server response with an action within a second or you will be kocked back to the begining of the Labyrinth!
                \n\n
                \tWhat "actions" do you mean?  What do I send back to the server?
                \tActions include things like walking east or west... or attacking another player's script!! Yes we are working on SvS (Script vs. Script, also known as PVP).  There's treasure (flags), loot, and other goodies all hidden within the Labyrinth.  As a human you can't navigate the Labyrinth fast enough... but your script CAN!!!
                \n\n
                \tI'm interested, how do I find out more?
                \tPing any of the BioHacking Village volunteers and tell them you are interested in running the Labyrinth!  We'll let you know where the next BHV CTF will be held!
            """
            return(F"<h1>View Page Source to see this page properly!</h1>\n<!--\n{welcome}\n{motd} -->")
        else: 
            abort(404)

    @app.route(F'{ROUTE_PREFIX}{VULN_ROUTE_PREFIX}/<vuln_endpoint>')
    @vulnerable
    def vuln_001(dbconn, vuln_endpoint, exploit_fields, vuln_query, vuln_args):
        result = None
        try:
            if exploit_fields is not None:
                query = vuln_query
                ex_values = []
                for exploit_field in exploit_fields:
                    ex_value = vuln_args.get(exploit_field, None)
                    if ex_value is None:
                        raise ValueError("Required Parameter Not Provided: {exploit_field}")
                    ex_values.append(ex_value)

                query = query.format(*ex_values)

                log.info(F"vuln_001: Executing vulnerable query: {query}")
                cursor = dbconn.executeVulnQuery(query, vuln_args)

                result = []
                for row in cursor:
                    result.append(row)
            else:
                abort(500)

            result = { "success": True, "result": result }
        except Exception as e:
            log.warning(F"vuln_001: User caused an exception: {e}")
            trace = traceback.format_exc()
            log.warning(F"{trace}")
            abort(500)
        return result

    @app.route(F'{ROUTE_PREFIX}/')
    def index():
        if 'username' in session:
            return 'Logged in as %s' % escape(session['username'])
        return 'You are not logged in'

    @app.route(F'{ROUTE_PREFIX}/logout')
    def logout():
        # remove the username from the session if it's there
        session.pop('user_id', None)
        return json.dumps({ "success": True })

    @app.route(F'{ROUTE_PREFIX}/updatePulseOx', methods=['GET'])
    def updatePulseOx():
        result = None
        try:
            #data = request.json
            data = request.args
            po_id = data.get("id", None)
            po_value = data.get("value", None)
            spo2_value = data.get("spo2", None)
            protocol = data.get("protocol", "0")

            print(F"The User submitted data: {json.dumps(data)}")
            if po_id is None:
                return json.dumps({ "success": False, "Error": "You must supply an id parameter to update the pulseox!"})
            if po_value is None:
                return json.dumps({ "success": False, "Error": "You must supply a pulse INT using the value parameter!"})
            if spo2_value is None:
                return json.dumps({ "success": False, "Error": "You must supply a spo2 INT to update the pulseox!"})
            
            if protocol == "0":
                po_value = int(po_value)
                spo2_value = int(spo2_value)
            elif protocol == "1":
                po_id = str(base64.a85decode(po_id).decode())
                po_value = int(base64.a85decode(po_value))
                spo2_value = int(base64.a85decode(spo2_value))

            print(F"User Data After{po_id},{po_value},{spo2_value}")
            ts = time.time()
            timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

            # This is the basic ID attack.
            if protocol == "0" and po_id == "1337" and po_value == 60 and spo2_value == 97:
                result = { "success" : "FLAG{PLACEHOLDER01}"}
                return json.dumps(result)
            elif protocol == "1" and po_id == "31337" and po_value == 120 and spo2_value == 80:
                result = { "success" : "FLAG{PLACEHOLDER08}"}
                return json.dumps(result)

            #Other Ideas....
                #Symetric Crypto Key
                #Add some checksums into the data so they have to sneak by the checksum
                #Guess some sort of auth token
            else:
                query = "SELECT id from oximeters where id=%s"
                results = _dbu.execDBPoolQuery(DB.READ, query, params=(po_id,))

                ids = []
                for row in results:
                    ids.append(row[0])

                if int(po_id) in ids:
                    query = "INSERT INTO oximeter_details values (NULL, %s, %s, %s, %s)"

                    results = _dbu.execDBPoolQuery(DB.WRITE, query, params=(po_id, timestamp, spo2_value, po_value))
                    if results.rowcount > 0:
                        result = { "success": True }
                else:
                    print(F"po_id: {po_id} is not in {json.dumps(ids)}")
                    result = { "success": False }
        except Exception as e:
            print(F"Failed to update pulseox data.  Exception was: {e}")
            result = { "success": False }

        return json.dumps(result)

    @app.route(F'{ROUTE_PREFIX}/getPulseOxDetails', methods=['GET'])
    @login_required
    def getPulseOxDetails():
        po_deets = []
        try:
            ox_id = request.args['ox_id']
            print(F"Request for Details on OX ID: {ox_id}")
            query = """SELECT timestamp, oxygen_level from oximeter_details WHERE oximeter_id = %s ORDER BY timestamp DESC limit 10"""
            results = _dbu.execDBPoolQuery(DB.READ, query, params=(ox_id,))
            for row in results:
                po_deets.append({ "timestamp":row[0].isoformat(), "oxygen_level":row[1] })
        except Exception as e:
            print(F'Failed to retrieve details for ID X.  Exception was: {e}')
        return json.dumps(po_deets)


    @app.route(F'{ROUTE_PREFIX}/getPulseOxList')
    @login_required
    def getPulseOxList():
        polist = []
        try:
            query = "SELECT id, serialnumber, name from oximeters;"
            results = _dbu.execDBPoolQuery(DB.READ, query)
            for row in results:
                polist.append({ "id":row[0], "serialnumber":row[1], "name":row[2] })
        except Exception as e:
            print(F"Failed to get the PulseOxList. Exception was: {e}")
        return json.dumps(polist)

    @app.route(F'{ROUTE_PREFIX}/getCurrentProfile', methods=['GET'])
    @login_required
    def getCurrentProfile():
        result = None
        try:
            print(F"CurrentUser: {g.user}")
            if g.user is not None:
                query = "SELECT id, username from users where username=%s"
                results = _dbu.execDBPoolQuery(DB.READ, query, (g.user,))
                ids = []
                for row in results:
                    ids.append({ "id": row[0], "username": row[1] })

                print(F"Possible User Matches: {json.dumps(ids)}")
                for user in ids:
                    if user["username"] == g.user:
                        query = "SELECT * from profiles where userid=%s"
                        results = _dbu.execDBPoolQuery(DB.READ, query, (user["id"],))
                        for row in results:
                            user_profile = { "userid": row[1], "username": g.user, "first_name": row[2], "last_name": row[3], "title": row[4], "notes": row[5]   }
                            result = { "success": True, "profile": user_profile }
                            print(F"Found user: {json.dumps(result)}")
                            break
                        break
            else:
                result = { "success": False }
        except Exception as e:
            print(F"Failed to get current Profile.  Exception was: {e}")
            result = { "success": False }
        return json.dumps(result)

    @app.route(F'{ROUTE_PREFIX}/getCMSVersion', methods=['GET'])
    def getCMSVersion():
        results = []
        page_name = request.args['page_name']
        version = request.args['version']
        try:
            query = "SELECT id, name, version, markdown, enabled, nav_icon, nav_order, nav_link, nav_text from cms WHERE name = %s and version = %s"
            results = _dbu.execDBPoolQuery(DB.READ, query, params=(page_name,version))
            if len(results) > 1:
                print(F'There is more than one CMS page for name {page_name} with version {version}')
            for row in results:    
                results.append( { 
                    "id": row[0], 
                    "name": row[1], 
                    "version": row[2], 
                    "markdown": row[3],
                    "enabled": row[4],
                    "nav_icon": row[5],
                    "nav_order": row[6],
                    "nav_link": row[7],
                    "nav_text": row[8]
                    } )
        except Exception as e:
            print(F'Failed to retrieve the CMS page {page_name} version {version}.  Exception was: {e}')
        return json.dumps(results)


    @app.route(F'{ROUTE_PREFIX}/getCMSCurrentPage', methods=['GET'])
    def getCMSCurrentPage():
        page_name = request.args['page_name']
        page = "";
        try:
            query = "SELECT id, name, version, markdown, nav_text, nav_icon, enabled, nav_order, nav_link from cms WHERE name = %s and enabled = 1 ORDER BY version DESC limit 1"
            results = _dbu.execDBPoolQuery(DB.READ, query, params=(page_name,))
            for row in results:
                page = { "id": row[0], "name": row[1], "version": row[2], "markdown": row[3], "nav_text": row[4], "nav_icon": row[5], "enabled": row[6], "nav_order": row[7], "nav_link": row[8] }
                break
        except Exception as e:
            print(F'Failed to retrieve the curernt page for {page_name}.  Exception was {e}')
        return json.dumps(page)

    @app.route(F'{ROUTE_PREFIX}/getNavLinks')
    def getNavLinks():
        nav_links = []
        try:
            query = 'SELECT name, nav_text, nav_icon, nav_order, nav_link from cms WHERE enabled = 1 ORDER BY nav_order'
            results = _dbu.execDBPoolQuery(DB.READ, query)
            for row in results:
                nav_links.append({ "name": row[0], "nav_text": row[1], "nav_icon": row[2], "nav_order": row[3], "nav_link": row[4] })
        except Exception as e:
            print(F'Failed to get NAV links, returning empty array.  Exception was: {e}')
            nav_links = []
        return json.dumps(nav_links)

    @app.route(F'{ROUTE_PREFIX}/saveCMS', methods=['POST'])
    def saveCMS():
        abort(404)
        result = None
        print('Saving CMS.')
        try:
            data = request.json
            cms_topic = data.get('cms_topic')
            new_cms_topic = data.get('new_cms_topic')
            cms_markdown = data.get('cms_markdown')
            cms_nav_enabled = data.get('cms_nav_enabled')
            print(F"cms_nav_enabled: {cms_nav_enabled}")
            cms_nav_enabled = 1 if cms_nav_enabled == True else 0
            cms_nav_text = data.get('cms_nav_text')
            cms_nav_icon = data.get('cms_nav_icon')
            cms_nav_order = data.get('cms_nav_order')
            cms_nav_link = data.get('cms_nav_link')
            if (cms_topic == "-1"):
                cms_topic = new_cms_topic

            query = "UPDATE cms SET enabled=0 WHERE name=%s"
            results = _dbu.execDBPoolQuery(DB.WRITE, query, (cms_topic,))
            if results.rowcount > 0:
                print(F"Disabled prior versions of {cms_topic}")
            else:
                print(F"No prior versions of {cms_topic} to disable.")

            query = "INSERT INTO cms (id, name, version, nav_text, nav_icon, enabled, markdown, nav_order, nav_link) values (NULL,%s,%s,%s,%s,%s,%s,%s,%s)"
            
            results = _dbu.execDBPoolQuery(DB.WRITE, query, (cms_topic, 1, cms_nav_text, cms_nav_icon, cms_nav_enabled, cms_markdown, cms_nav_order, cms_nav_link))
            if results.rowcount > 0:
                result = {"success": True }
                _db_conns[DB.WRITE].commit()
            else:
                result = {"success": False }
                #TODO: Add rollback
                #_db_conns[DB.WRITE].rollback()
        except Exception as e:
            print(F'Failed to save CMS page for name {cms_topic}.  Exception was {e}')
            result = { "success": False }
        return json.dumps(result)

    @app.route(F'{ROUTE_PREFIX}/getCMSPagesList', methods=['GET'])
    def getCMSPagesList():
        page_list = []
        try:
            query = "SELECT DISTINCT name from cms WHERE enabled=1 ORDER BY version DESC"
            results = _dbu.execDBPoolQuery(DB.READ, query)
            for row in results:
                page_list.append(row[0])
        except Exception as e:
            print(F'Failed to get the CMS pages list.  Returning empty array.  Exception was {e}')
            page_list = []
        return json.dumps(page_list)


    @app.route(F'{ROUTE_PREFIX}/login', methods=['POST'])
    def Authenticate():
        print("Authenticating...")
        data = request.json
        username = data.get('username', None)
        password = data.get('password', None)

        if username is not None and password is not None:
            query = "SELECT username, password, role from users WHERE username=%s"
            results = _dbu.execDBPoolQuery(DB.READ, query, (username,))
            for row in results:
                if (username == row[0]):
                    md5sum = hashlib.md5(password.encode())
                    md5sum = md5sum.hexdigest()
                    print(F"Checking md5sum {md5sum} against value {row[1]}...")
                    if (md5sum == row[1]):
                        session["user_id"] = username
                        return { "success": True }
            return { "success": False }
        else:
            return { "success": False }

    good_to_go = configSanityCheck(_config)

    if good_to_go:
        return app
    else:
        print("Your config.json didn't pass the pre-flight sanity check.\nPlease review your config.json file which is normally located at: api/src/openpulseox_api/config/config.json\nBailing out...")
        sys.exit(1)

app = None
if (__name__ == "__main__"):
    print("Starting in __main__ mode...")
    app = create_app()

if (__name__=="openpulseox_api.app"):
    print("Starting uwsgi mode...")
    
    #app = create_app()

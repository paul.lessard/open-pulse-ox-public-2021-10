#Copyright © 2021 Paul Lessard (a.k.a. Battlechef #1666)
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
#associated documentation files (the “Software”), to deal in the Software without restriction, 
#including without limitation the rights to use, copy, modify, merge, publish, distribute, 
#sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or 
#substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
#NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES 
#OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


class BasePlugin():
    def __init__(self, app, config, name, ROUTE_PREFIX):
        if app is None:
            raise ValueError(F"You must pass in a valid flask app to run plugin {name}!")
        self._app = app
        self._config = config
        self._name = name
        self._ROUTE_PREFIX = ROUTE_PREFIX
        self.initPlugin()

    #Using this should allow you to specify which openpulseox-api function you want to patch.  That is, you can wholesale replace an internal function using this call.
    #  This is intended for those that want total control over the API and thus is more for advanced use cases and for futureproofing the api a little... like adding things I never thought of.  
    #  You should probably use any other function if you can before trying to use this.  Since I can't anticipate what you will overwrite, I could end up breaking your patch in future releases if you use this.  --Battlechef
    def patchFunction(self):
        pass

    #Should let you add a new route pointing to your function.  This lets you add net new capabilities to the openpulseox app.  Note that this is a big kids function... meaning your route will be called with very little checking (if any).
    #  This means you need to handle all the security checks, protections against injection,  and all the other bits yourself.
    def addRoute(self, route_endpoint, route_callback):
        if self._app is not None:
            self._app.add_url_rule(route_endpoint, view_func=route_callback)
        else:
            raise ValueError(F"Cannot execute plugin: {self._name}.addRoute.  self._app is None! ")

    #Lets you add a net new vulnerable function/route to the openpulseox app.
    def addVulnRoute(self):
        pass

    #Should let you add a net new authentication mechanism to the application.  Should let you replace the UI login form, and the API authentication mechanism.
    def addAuthentication(self):
        pass


    #Override this to run your own code upon plugin load.
    def initPlugin(self):
        pass

    def getConfig(self):
        return self._config


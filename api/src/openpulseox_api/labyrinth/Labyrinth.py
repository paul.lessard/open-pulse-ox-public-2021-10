#Copyright © 2021 Paul Lessard (a.k.a. Battlechef #1666)
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
#associated documentation files (the “Software”), to deal in the Software without restriction, 
#including without limitation the rights to use, copy, modify, merge, publish, distribute, 
#sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or 
#substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
#NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES 
#OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

class Labyrinth:
    def __init__(self, flask_app, labyrinth_config, labyrinth_key = None):
        super(self)
        self._app = flask_app

        

        if labyrinth_key is not None:
            self._config = labyrinth_config[labyrinth_key]
        else:
            self._config = labyrinth_config
        


    @self._app.route(F'{self._config["LABYRINTH_PREFIX"]}')
    def interact():
        pass

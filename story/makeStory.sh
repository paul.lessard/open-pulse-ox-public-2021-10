#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

function usage {
	echo "Usage: $0 clean|prepare|makestory|go"
}	

function clean {
	echo 'Cleaning...'
	rm -fR ../build
}

function prepare {
	echo 'Preparing for build...'
	mkdir ../build
	echo "Copying api..."
	cp -R ../api ../build
	echo "Copying ui..."
	cp -R ../ui ../build
}

function makestory {
	echo "Running Storymaker build..."
	python3 Storymaker.py ./
}

if [ $# -lt 1 ]
then
	usage
else
	case $1 in
		"clean")
			clean
			;;
		"prepare")
			prepare
			;;
		"makestory")
			makestory
			;;
		"go")
			clean
			prepare
			makestory
			;;
		*)
			usage
			;;
	esac
fi



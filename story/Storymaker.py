import json
import argparse
import os
from getpass import getpass
import mysql.connector
import shutil
from storymaker import PackImage
import traceback
import collections.abc
import hashlib

parser = argparse.ArgumentParser(description='Performs various Openpulseox application setup steps.')
parser.add_argument('path', type=str, help='The full path of Storyfile.json')
parser.add_argument('--flags', dest='flags', action='store_const', const=True, help='Set flags mode.  Flags are expected to be in the openpulseox/build directory, which is created automatically using the makeStory.sh script.')
parser.add_argument('--placeholders', dest='placeholders', action='store_const', const=True, help='Uses the Storyfile.json to replace placeholders in various files before performing the build.  Useful for setting passwords and other bits that need to change from CTF to CTF.')
parser.add_argument('--db', dest='db', action='store_const', const=True, help='Creates database entries from the Storyfile.  This should only be run from inside the docker container.')
parser.add_argument('--oximeters', dest='oximeters', action='store_const', const=True, help='Creates database entries for the pulse oximeters from the Storyfile.  This should only be run from inside the docker container.')
parser.add_argument('--users', dest='users', action='store_const', const=True, help='Creates database user entries from the Storyfile.  This should only be run from inside the docker container.')
parser.add_argument('--pack_images', dest='pack_images', action='store_const', const=True, help='Creates packed images from source image files indicated in the Storyfile.')
parser.add_argument('--unix-auth', dest='unix_auth', action='store_const', const=True, help='Use Unix Socket authentication, which is the same as when you answer "n" to the "Use username/password auth?" prompt.')
parser.add_argument('--user-auth', dest='user_auth', action='store_const', const=True, help='Use mariadb username/password authentication, which is the same as when you answer "y" to the "Use username/password auth?" prompt.')
parser.add_argument('--api-config', type=str, help='Copy the config.json from the Storyfile.json into the file specified.  This generates the Openpulseox API config.json file.')
parser.add_argument('--base-dir', type=str, help='Use this as the working directory while processing the story.  Useful for --flags.')


args = parser.parse_args()

story = json.load(open(os.path.join(args.path, "Storyfile.json"), "r"))
print(F"Processing Story: '{story['Title']}'")

auth = None
if args.unix_auth:
    auth = "N"
elif args.user_auth:
    auth = "Y"


class PlaceholderPopulator:
    def getRandomHash(self, hash_type = "md5"):
        output = None
        try:
            rnd = os.urandom(60)

            hl =  hashlib.new(hash_type)
            hl.update(rnd)
            output = hl.hexdigest()
        except Exception as e:
            print(f"Failed to getRandomMD5.  Returning None.  Exception was: {e}")
            output = None
        return output







def dbConn(auth):
    try:
        if auth is None:
            print("Use username/password auth [y/n]?")
            auth = input()

        if (auth.upper() == "Y"):
            print("Enter DB Username: ")
            username = input()

            print("Enter DB Password: ")
            password = getpass()

            conn =  mysql.connector.connect(host="localhost", user=username, password=password, database="openpulseox")
        else:
            conn =  mysql.connector.connect(unix_socket="/run/mysqld/mysqld.sock", user="root", database="openpulseox")

        conn.autocommit = True
    except Exception as e:
        print(F"Failed to connecto to the database! Exception was: {e}")
    return conn


def updatePulseOx(story, args, conn = None):
    success = False
    try:
        oxs = story["oximeters"]

        cursor = conn.cursor()

        for name,ox in oxs.items():
            query = "INSERT INTO openpulseox.oximeters values (NULL, %s, %s)"
            serialnumber = ox["serialnumber"]

            cursor.execute(query, (serialnumber, name))
            row_id = cursor.lastrowid

            for detail in ox["details"]:
                query = "INSERT INTO openpulseox.oximeter_details values (NULL, %s, %s, %s, %s)"
                timestamp = detail["timestamp"]
                ox_lev = detail["oxygen_level"]
                pulse = detail["pulse_rate"]

                cursor.execute(query, (row_id, timestamp, ox_lev, pulse))
        success = True
    except Exception as e:
        print(F"Failed to updatePulseOx.  Exception was: {e}")
        success = False
    return success

def updateCMS(story, args, conn = None):
    success = False
    try:
        cursor = conn.cursor()
        for page in story["cms"]:
            print(F"Processing CMS page: {json.dumps(page)}")
            for version in story["cms"][page]:
                query = "INSERT INTO cms values (NULL, %s, %s, %s, %s, %s, %s, %s, %s)"
                resuts = cursor.execute(query, (
                    version["name"]
                    , version["version"]
                    , version["nav_text"]
                    , version["nav_icon"]
                    , version["enabled"]
                    , version["markdown"]
                    , version["nav_order"]
                    , version["nav_link"]
                    ))
        success = True
    except Exception as e:
        print(F"Failed to updateCMS.  Exception was: {e}")
        success = False

    return success

def updateVulnUsers(story, args, conn = None):
    success = False
    try:
        cursor = conn.cursor()
        for vuln_name,vuln_config in story["config.json"]["vuln_endpoints"].items():
            print(F"Processing vuln: {vuln_name}")
            create_query = "CREATE USER IF NOT EXISTS %s@localhost IDENTIFIED BY %s"

            #gotta have the z or it don't work!!! ---|
            #       \/-------------------------------|
            vuln_credz = vuln_config["vuln_credentials"]
            grant_queries = vuln_credz["grants"]
            
            cursor.execute(create_query, (
                vuln_credz["username"]
                , vuln_credz["password"]
                ))

            for grant in grant_queries:
                cursor.execute(grant, (vuln_credz["username"],) )

            success = True
    except Exception as e:
        print(F"Failed to updateVulnUsers: Exception was: {e}")
        success = False
    return success


def updateDB(story, args, conn = None):
    print(F"Updating CMS...")
    result = updateCMS(story, args, conn)
    print(F"CMS: {result}")
    print(F"Updating Vuln Users...")
    result = updateVulnUsers(story, args, conn)
    print(F"Vuln Users: {result}")

def update(d, u, idx = 0):
    if u[idx].isnumeric():
        k = int(u[idx])
    else:
        k = u[idx]

    if idx < len(u) - 1:
        d[k] = update(d[k], u, idx+1)
    else:
        return u[idx] 

    return d

def updatePlaceHolderInConfig(filename, config_path, flag_placeholder, flag):
    success = False
    try:
        print(F"Updating config_path: {config_path}, placeholder: {flag_placeholder}, real flag: {flag}")
        config = json.load(open(filename, "r"))
        ptr = config
        path = config_path.split(".")
        for p in path:
            if p.isnumeric():
                ptr = ptr[int(p)]
            else:
                ptr = ptr[p]
        

        print(F"""
                Value Changed: 
                Old:
                {ptr}""")
                

        newvalue = ptr.replace(flag_placeholder, flag)

        path.append(newvalue)
        update(config, path)

        print(F"""New:
                {newvalue}""")
        print(json.dumps(config))

        outfile = F"{filename}.tmp"
        json.dump(config, open(outfile, "w"))
        shutil.move(outfile, filename)

        success = True
    except Exception as e:
        trace = traceback.format_exc()
        print(F"Failed to update flag in config.  Exception was: {e}")
        print(F"{trace}")
        success = False
    return success

def updatePlaceHolderInFile(filename, flag_placeholder, flag):
    success = False
    try:
        print(F"Updating file: {filename}, placeholder: {flag_placeholder}, real flag: {flag}")
        with open(filename, "r") as source_file:
            with open(F"{filename}.tmp", "w") as dest_file:
                for line in source_file:
                    if flag_placeholder in line:
                        line = line.replace(flag_placeholder, flag)
                    dest_file.write(line)
        
        shutil.move(F"{filename}.tmp", filename)

        success = True
    except Exception as e:
        trace = traceback.format_exc()
        print(F"Failed to update flag in file.  Exception was: {e}")
        print(F"{trace}")
        success = False
    return success

def updateFlags(story, args):
    flags = story["flags"]
    base_dir = args.base_dir

    for flag in flags:
        flag_file = flag.get("file", None)
        config_path = flag.get("config", None)
        flag_placeholder = flag["placeholder"]
        flag_flag = flag["flag"]

        if flag_file is not None:
            filename = flag_file
            if base_dir is not None:
                filename=os.path.join(base_dir, flag_file)

            updatePlaceHolderInFile(filename, flag_placeholder, flag_flag)
        if config_path is not None:
            filename = "story/Storyfile.json"
            if base_dir is not None:
                filename=os.path.join(base_dir, filename)

            updatePlaceHolderInConfig(filename, config_path, flag_placeholder, flag_flag)


def updatePlaceHolders(story, args):
    ph = story["placeholders"]

    for flag in ph:
        flag_file = flag.get("file", None)
        if ";" in flag_file:
            flag_file = flag_file.split(";")
        else:
            flag_file = [ flag_file ]


        config_path = flag.get("config", None)
        flag_placeholder = flag["placeholder"]
        flag_flag = flag["real_value"]

        if flag_file is not None:
            for filename in flag_file:
                updatePlaceHolderInFile(filename, flag_placeholder, flag_flag)

        if config_path is not None:
            filename = "devBuild/story/Storyfile.json"
            updatePlaceHolderInConfig(filename, config_path, flag_placeholder, flag_flag)


def updateUsers(story, args, conn = None):
    cursor = conn.cursor()
    users = story["users"]
    for user in users:
        query = "INSERT INTO users values (NULL, %s, %s, %s)"
        results = cursor.execute(query, (
                user["username"],
                user["role"],
                hashlib.md5(user["password"].encode()).hexdigest()
                ))
        
        userid = cursor.lastrowid
        profile = user["profile"]
        query = "INSERT INTO profiles values (NULL, %s, %s, %s, %s, %s)"
        results = cursor.execute(query, (
                userid
                ,profile["first_name"]
                ,profile["last_name"]
                ,profile["title"]
                ,profile["notes"]
                ))


def flush_perms(conn):
    cursor = conn.cursor()
    cursor.execute("FLUSH PRIVILEGES")


def getConn(conn):
    if conn is None:
        conn = dbConn(auth)
    return conn

conn = None

if args.flags:
    print(F"Performing flag replacements...")
    updateFlags(story, args)

if args.flags:
    print(F"Performing placeholder replacements...")
    updatePlaceHolders(story, args)

if args.db:
    print(F"Performing database population...")
    conn=getConn(conn)
    updateDB(story, args, conn)
    flush_perms(conn)

if args.oximeters:
    print(F"Performing oximeter population...")
    conn=getConn(conn)
    updatePulseOx(story, args, conn)
    flush_perms(conn)

if args.users:
    print(F"Performing database user population...")
    conn=getConn(conn)
    updateUsers(story, args, conn)

if args.pack_images:
    print(F"Performing image packing...")
    if "PackImages" in story:
        pack_images = story["PackImages"]["dotted_notation"]
        PackImage.dottedPack(pack_images)

if args.api_config:
   print(F"Dumping config.json to {args.api_config}...")
   config = story["config.json"]
   with open(args.api_config, 'w') as configfile:
       json.dump(config, configfile)

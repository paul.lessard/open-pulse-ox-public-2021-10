from PIL.PngImagePlugin import PngImageFile, PngInfo
import argparse
from zipfile import ZipFile
import zipfile
import base64
import json
from typing import *
from enum import Enum

DEBUG = False
_intermediateData = None

class StrEnum (str, Enum):
   pass 

class UPDATE_TYPES(StrEnum):
    LOAD_DATA= "LOAD_DATA: {message}"
    DATA_CONVERT= "DATA_CONVERT: {message}"
    BASE64= "BASE64: {message}"
    ADD_TEXT= "ADD_IMG_TEXT: {message}"
    ZIP= "ZIP: {message}"
    DEBUG= "DEBUG: {message}"

def storeIntermediateData(data):
    global _intermediateData
    _intermediateData = data

def printUpdate(msg_type:UPDATE_TYPES, message:str):
    try:
        msg_str = msg_type
        print(msg_str.format(message = message))
    except Exception as e:
        print(F"Failed to print update.  Exception was: {e}")


def getDataFromFile(data: List[Any], params: Dict[str,Any], callback: Callable[[Any], Any] = None):
    result = data
    try:
        mode = params.get("mode", 'rb')
        for filename in params.get("source_files", []):
            with open(filename, mode) as infile:

                printUpdate(UPDATE_TYPES.LOAD_DATA, F"Getting data from file: {filename}")
                result.append(infile.read())
        if callback is not None and callable(callback):
            callback(result)
    except Exception as e:
        print(F"Failed to obtain data from file.  Exception was: {e}")
    return result

def getBase64(data: List[Any], params: Dict[str,Any], callback: Callable[[Any],Any] = None):
    result = []
    try:
        base64_decode = params.get("decode", False)

        if not base64_decode:
            for indata in data:
                printUpdate(UPDATE_TYPES.BASE64, F"Performing base64 encode on data index: {data.index(indata)}")
                if isinstance(indata, str):
                    indata = indata.encode()
                result.append(base64.b64encode(indata))
        else:
            for indata in data:
                printUpdate(UPDATE_TYPES.BASE64, F"Performing base64 decode on data index: {data.index(indata)}")
                if isinstance(indata, str):
                    indata = indata.encode()
                result.append(base64.b64decode(indata))

        if callback is not None and callable(callback):
            callback(result)
    except Exception as e:
        print(F"Failed to obtain base64 from input.  Exception was: {e}")
    return result

def getDataAsString(data: List[Any], params: Dict[str,Any], callback: Callable[[Any], Any] = None):
    result = []
    try:
        for indata in data:
            printUpdate(UPDATE_TYPES.DATA_CONVERT, F"Converting data to string in data index: {data.index(indata)}")
            result.append(indata.decode('utf-8'))
        result.append(new_data)
        if callback is not None and callable(callback):
            callback(result)
    except Exception as e:
        print(F"Failed to obtain get data as string from input.  Exception was: {e}")
    return result

def getConcatenation(data: List[Any], params: Dict[str,Any], callback: Callable[[Any], Any] = None):
    result = []
    try:
        new_data = ""
        for indata in data:
            printUpdate(UPDATE_TYPES.DATA_CONVERT, F"Concatenating data index: {data.index(indata)}")
            if isinstance(indata, str):
                pass
            else:
                indata = indata.decode('utf-8')
            new_data = F"{new_data}{indata}"
        result.append(new_data)
        if callback is not None and callable(callback):
            callback(result)
    except Exception as e:
        print(F"Failed to obtain concatenation from input.  Exception was: {e}")
    return result

def addAncillaryText(data: List[Any], params: Dict[str,Any], callback: Callable[[Any],Any] = None):
    result = []
    try:
        source_files = params["source_files"]
        dest_files = params["dest_files"]
        
        imgs = []
        for filename in source_files:
            try:
                printUpdate(UPDATE_TYPES.ADD_TEXT, F"Loading source_file: {filename}")
                img = PngImageFile(filename)
                imgs.append(img)
            except Exception as e:
                print(F"Could not load an image from file.  Skipping file: {filename}.  Exception was: {e}")
        
        keys = params["key"]
        idx = 0
        for img in imgs:
            metadata = PngInfo()
            cur_dat_idx = min(len(data) - 1, idx)
            message = data[cur_dat_idx]

            cur_dest_idx = min(len(dest_files) - 1, idx)
            dest_file = dest_files[cur_dest_idx]
            
            cur_key_idx = min(len(keys) - 1, idx)
            key = keys[cur_key_idx]

            metadata.add_text(key, message)

            printUpdate(UPDATE_TYPES.ADD_TEXT, F"Saving new img to dest_file: {dest_file}")
            img.save(dest_file, pnginfo=metadata)
            idx += 1

        if callback is not None and callable(callback ):
            callback(results)
    except Exception as e:
        print(F"Failed to add ancillary chunk from input.  Exception was: {e}")
    return result

def addDataToZip(data_filename, data, zip_file, overwrite = False):
    success = False
    try:
        mode = 'w' if overwrite else 'a'
        with ZipFile(zip_file, mode, compression= zipfile.ZIP_LZMA) as myzip:
            printUpdate(UPDATE_TYPES.ZIP, F"Saving string to file: {data_filename}; and adding to zip file: {zip_file}")
            myzip.writestr(data_filename, data)

        success = True
    except Exception as e:
        print(F"Failed to add data to zip file.  Exception was: {e}")
        success = False
    return success

def addToZip(data: List[Any], params: Dict[str,Any], callback: Callable[[Any],Any] = None):
    result = data
    try:
        overwrite = params.get("overwrite", False);
        if isinstance(params["dest_files"], str) or len(params["dest_files"]) == 1:
            zip_file = params["dest_files"]
            zip_file = zip_file if isinstance(zip_file, str) else zip_file[0]
            printUpdate(UPDATE_TYPES.ZIP, F"Adding all data to the dest_file: {zip_file}")
            if "file_prefix" in params:
                file_prefix = params["file_prefix"]
                file_extension = params.get("file_extension", "txt")

                count = 0
                for indata in data:
                    addDataToZip(F"{file_prefix}{count}.{file_extension}", indata, zip_file, overwrite = overwrite)
                    count += 1

        elif len(params["dest_files"]) == len(data):
            printUpdate(UPDATE_TYPE.ZIP, "Adding each data index to each dest_file")

        if callback is not None and callable(callback ):
            callback(results)
    except Exception as e:
        print(F"Failed to add to zip file.  Exception was: {e}")
    return result

def base64File(in_files: List[str], out_file: str, overwrite = False):
    try:
        mode = 'w' if overwrite else 'wa'
        encoded: List[str] = []
        for filename in in_files:
        
            with open(filename, 'r') as prebase64:
                data = prebase64.read()
                encoded.append(base64.b64encode(data).decode("utf-8"))
            with open(out_file, mode) as postbase64:
                map(lambda x: postbase64.write(x), encoded)

    except Exception as e:
        print(F"Failed to add files ({json.dumps(in_files)}) to zip {out_file}.  Exception was: {e}")

def loadStrings(data: List[Any], params: Dict[str,Any], callback: Callable[[Any], Any] = None):
    results = data
    try:
        for in_str in params.get("in_strings", []):
            results.append(in_str)
            printUpdate(UPDATE_TYPES.LOAD_DATA, F"Loaded string into data buffer index: {data.index(in_str)}.")
        if callback is not None and callable(callback ):
            callback(results)
    except Exception as e:
        print(F"Failed to load strings from dotted params.  Exception was: {e}")
    return results

def clearBuffer(data: List[Any], params: Dict[str,Any], callback: Callable[[Any], Any] = None):
    if callback is not None and callable(callback ):
        callback([])
    return []

def outputDebug(data: List[Any], params: Dict[str,Any], callback: Callable[[Any], Any] = None):
    results = data
    try:
        tmp_str = ""
        for indata in data:
            idx = data.index(indata)
            tmp_str = indata
            if isinstance(tmp_str, str):
                tmp_str = indata
            else:
                tmp_str = indata.decode('utf-8')
            printUpdate(UPDATE_TYPES.DEBUG, F"Outputing data  index [{idx}] as a UTF-8 string:\n{tmp_str}")
    except Exception as e:
        print(F"Failed to output data for debug info.  Exception was: {e}")
    return results

def addFilesToZip(in_files: List[str], out_file: str, overwrite = False):
    try:
        mode = 'w' if overwrite else 'wa'
        with ZipFile(out_file, mode, compression= zipfile.ZIP_LZMA) as myzip:
            for filename in in_files:
                myzip.write(filename)
    except Exception as e:
        print(F"Failed to add files ({json.dumps(in_files)}) to zip {out_file}.  Exception was: {e}")
    

def performDotTask(dot, params, data: List[Any]):
    result = None
    try:
        if DEBUG==True:
            printUpdate(UPDATE_TYPES.DEBUG, F"Dot: {dot}; Params: {json.dumps(params)}")
            printUpdate(UPDATE_TYPES.DEBUG, F"Data: {json.dumps(data)}")
        if dot == "data_file":
            result = getDataFromFile(data, params)
        if dot == "base64":
            result = getBase64(data, params)
        if dot == "load_str":
            result = loadStrings(data, params)
        if dot == "print":
            result = outputDebug(data, params)
        if dot == "zip_data":
            result = addToZip(data, params)
        if dot == "add_img_text":
            result = addAncillaryText(data, params)
        if dot == "clear":
            result = clearBuffer(data, params)
    except Exception as e:
        print(F"Failed dot task ({dot}) with current input. Exception was {e}")
    return result

def dottedPack(dotted_notation):
    try:
        dots = dotted_notation 

        data = []

        for directive in dots:
            for (dot, params) in directive.items():
                data = performDotTask(dot, params, data)
    except Exception as e:
        print(F"Failed to perform dotted pack.  Exception was: {e}")


################# ARGUMENT HANDLING ########################

def getJSONBaseKey(key, json_data):
    keys = key.split('.')
    sub_tree = json_data
    for key in keys:
        sub_tree = sub_tree.get(key, None)
        if sub_tree == None:
            raise(F'Key not found, returning none. Check your params. Key: {key}')
    return sub_tree

def getDottedNotation(args):
    dotted_notation = None
    if args.dotted_notation_file:
        note_file = args.dotted_notation_file
        base_key = None
        
        if ":" in note_file:
            (note_file, base_key) = note_file.split(":")
        dotted_notation = json.load(open(note_file, 'r'))
        
        if base_key is not None:
            dotted_notation = getJSONBaseKey(base_key, dotted_notation)
            
    elif args.dotted_notation:
        dotted_notation = args.dotted_notation
    else:
        print("No dotted notation argument supplied.  Please see help with --help.")

    if isinstance(dotted_notation, str):
        dotted_notation = json.loads(dotted_notation)

    return dotted_notation

if (__name__ == "__main__"):
    parser = argparse.ArgumentParser(description='Pack PNG ancillary chunks with specified data.  You MUST supply one of the dotted-notation arguments AND one of the dotted-params.')
    parser.add_argument('--dotted-notation', type=str, help='The list of operations to be performed to --source_data separated by dots.  For a list of supported operations.  See DottedNotationOptions in this script.')
    parser.add_argument('--dotted-notation-file', type=str, help='The file containing the list of operations to be performed to --source_data separated by dots.  For a list of supported operations.  See DottedNotationOptions in this script.')

    args = parser.parse_args()

    dotted_notation = getDottedNotation(args)

    dottedPack(dotted_notation)

from openpulseox_api.plugins.baseplugin import BasePlugin
from flask import request

class TestPlugin(BasePlugin):
    def initPlugin(self):
        self.addRoute("/api/testplugin", self.echoTest)

    def echoTest(self, *args, **kwargs):
        result = None
        try:
            data = request.args.get("echo", None)
            if data is None:
                result = F"You must specify parameter 'echo'."
            else:
                result = F"You said: {data}"
        except Exception as e:
            print(F"{self._name}: Failed to echoTest! {e}")
        return result

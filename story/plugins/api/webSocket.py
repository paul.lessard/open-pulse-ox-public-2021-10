from openpulseox_api.plugins.baseplugin import BasePlugin
from openpulseox_api.config.config import Config
from flask import request
from flask import Flask
import asyncio
import websockets

class SimpleSocket (BasePlugin):
    def run(self):
        start_server = websockets.unix_serve(self.echo, path="/run/ws_serve/ws.sock")

        asyncio.get_event_loop().run_until_complete(start_server)
        asyncio.get_event_loop().run_forever()

    async def echo(self, websocket, path):
        async for message in websocket:
            print(f"Path: {path}; Got a message: {message}")
            await websocket.send(message)



if __name__ == "__main__":
    app = Flask(__name__)
    ROUTE_PREFIX = "/ws/"

    config = Config()
    sock = SimpleSocket(app, config, __name__, ROUTE_PREFIX)
    sock.run()

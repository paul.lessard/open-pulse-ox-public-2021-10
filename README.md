Hey there!  Paul Lessard here, I'm just trying to add documentation as time allows to keep this from growing stale.

Multiple people helped out with things like story content and code contributions.  I will get their permission first, but I hope to add their names here!


This repo uses Docker to automatically build an alpine image for the openpulseox app.  Please run buildOpenPulseOx.sh to perform the image build.  For the most part this *should* autobuild itself once you have the dependencies installed.  The primary dependencies are a recent version of python3, nodejs >=14, create-react-app, and react-scripts.  This is not an exhaustive list though, please see dependencies.txt in the root of this project for the offical list of dependencies.  Unfortunately it is not yet self installing, you might have to do a bit of looking depending on what OS you are using.  This is currently developed and tested as working on Ubuntu v20.


##Building a development container##

To lauch the build, cd into the root of this repo and run:

`./buildOpenPulseOx.sh --clean`

Then:

`./buildOpenPulseOx.sh --generate-dev`


There is a bug in this version with buildOpenPulseOx not correctly setting up the devBuild folder on a brand new repo clone.  However, running --clean first should set the directory up correctly.


This will take some time, but should produce a docker image called "openpulseox".  To run this container, perform these steps:


#If you are developing on your own machine, i.e. not a multiuser environment run:
`./runDockerDev.sh`
#Then from within the container run:
`/devtest.sh go`

#If you are developing on a server or multiuser environment, this one is probably better:
`./runDockerDev.sh -d`
#The above starts the container "detached" so it runs in the background, it also runs '/devtest.sh go' for you.
# or, if you don't want to expose the docker container to your network/the internet use this instead:
`./runDockerDev.sh -ls`

This should start the container in a dev state.  The final prod build will autostart all services, and auto generate the story using the Storyfile.json file located in ./story/

##Building a prod container##
To lauch the build, cd into the root of this repo and run:
`./buildOpenPulseOx.sh --generate-prod`


This will take some time, but should produce a docker image called "openpulseox".  To run this container, perform these steps:

`docker run -dit openpulseox`

This should start the container in a dev state.  The final prod build will autostart all services, and auto generate the story using the Storyfile.json file located in ./story/

